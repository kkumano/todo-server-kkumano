require 'rails_helper'

RSpec.describe "Todos", type: :request do

  # initialize test data 
  let!(:todos) { create_list(:todo, 10) }

  # Test suite for GET /todos
  describe 'GET /todos' do
    example '一覧の取得成功' do
      get '/todos'

      expect(json).to be_present
      expect(json['todos'].size).to eq 10
      expect(response.status).to eq 200
    end

    example 'サーバーサイドによる一覧の取得失敗' do
      allow(Todo).to receive(:select).and_raise(ActiveRecord::ActiveRecordError)
      get '/todos'

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 3
      expect(json['error_message']).to eq '一覧の取得に失敗しました'
    end

    example 'その他のエラーによる一覧の取得失敗' do
      allow(Todo).to receive(:select).and_raise(Exception)
      get '/todos'

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  # Test suite for POST /todos
  describe 'POST /todos' do
    # valid payload
    let(:valid_attributes) { { title: 'test', detail: 'test' } }
    let(:invalid_attributes) { { title: '' } }

    example 'Todoの登録成功' do
      post '/todos', params: valid_attributes

      expect(response.status).to eq 200
      expect(json['error_code']).to eq 0
      expect(json['error_message']).to eq ''
    end

    context 'title最大文字数丁度の場合' do
      let(:params) { { title: 'a' * 100 } }

      example 'Todoの登録成功' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'detail最大文字数丁度の場合' do
      let(:params) { { title: 'test', detail: 'a' * 1000 } }

      example 'Todoの登録成功' do
        post '/todos', params: params

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    example 'Todoの登録失敗' do
      post '/todos', params: invalid_attributes

      expect(response.status).to eq 400
      expect(json['error_code']).to eq 2
      expect(json['error_message']).to eq 'リクエストの形式が不正です'
    end

    context 'title最大文字数＋１の場合' do
      let(:params) { { title: 'a' * 101 } }

      example 'Todoの登録失敗' do
        post '/todos', params: params

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'detail最大文字数＋１の場合' do
      let(:params) { { title: 'test', detail: 'a' * 1001 } }

      example 'Todoの登録失敗' do
        post '/todos'

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    example 'サーバーサイドのエラーによるTodoの登録失敗' do
      allow(Todo).to receive(:create!).and_raise(ActiveRecord::ActiveRecordError)
      post '/todos', params: valid_attributes

      expect(response.status).to eq 500
      expect(json['error_code']).to eq 4
      expect(json['error_message']).to eq '登録に失敗しました'
    end

    example 'その他のエラーによるTodoの登録失敗' do
      allow(Todo).to receive(:create!).and_raise(Exception)
      post '/todos', params: valid_attributes

      expect(json['error_code']).to eq 1
      expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
    end
  end

  # Test suite for PUT /todos/:id
  describe 'PUT /todos/:id' do
    let(:valid_attributes) { { title: 'Shopping' } }
    let(:invalid_attributes) { { title: '' } }

    context 'データがある場合' do
      let(:todo_id) { todos.first.id }

      example 'Todoの更新成功' do
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end

      context 'title最大文字数丁度の場合' do
        let(:params) { { title: 'a' * 100 } }

        example 'Todoの更新成功' do
          post '/todos', params: params

          expect(response.status).to eq 200
          expect(json['error_code']).to eq 0
          expect(json['error_message']).to eq ''
        end
      end

      context 'detail最大文字数丁度の場合' do
        let(:params) { { title: 'test', detail: 'a' * 1000 } }

        example 'Todoの更新成功' do
          post '/todos', params: params

          expect(response.status).to eq 200
          expect(json['error_code']).to eq 0
          expect(json['error_message']).to eq ''
        end
      end

      example 'Todoの更新失敗' do
        put "/todos/#{todo_id}", params: invalid_attributes

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end

      context 'title最大文字数＋１の場合' do
        let(:params) { { title: 'a' * 101 } }

        example 'Todoの更新失敗' do
          post '/todos', params: params

          expect(response.status).to eq 400
          expect(json['error_code']).to eq 2
          expect(json['error_message']).to eq 'リクエストの形式が不正です'
        end
      end

      context 'detail最大文字数＋１の場合' do
        let(:params) { { title: 'test', detail: 'a' * 1001 } }

        example 'Todoの更新失敗' do
          post '/todos'

          expect(response.status).to eq 400
          expect(json['error_code']).to eq 2
          expect(json['error_message']).to eq 'リクエストの形式が不正です'
        end
      end
    end

    context 'データがない場合' do
      let(:todo_id) { todos.first.id }
      # 事前に該当のTodoレコードを削除する
      before { Todo.find(todo_id).destroy }

      example 'Todoの更新失敗' do
        put "/todos/#{todo_id}"

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバーサイドでエラーが起きた場合' do
      let(:todo_id) { todos.first.id }

      example 'Todoの更新失敗' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(ActiveRecord::ActiveRecordError)
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 5
        expect(json['error_message']).to eq '更新に失敗しました'
      end
    end

    context 'その他のサーバーサイドによるエラーが起きた場合' do
      let(:todo_id) { todos.first.id }

      example 'Todoの更新失敗' do
        allow_any_instance_of(Todo).to receive(:update!).and_raise(Exception)
        put "/todos/#{todo_id}", params: valid_attributes

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end

  # Test suite for DELETE /todos/:id
  describe 'DELETE /todos/:id' do
    context 'データがある場合' do
      let(:todo_id) { todos.first.id }

      example 'Todoの削除成功' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 200
        expect(json['error_code']).to eq 0
        expect(json['error_message']).to eq ''
      end
    end

    context 'データがない場合' do
      let(:todo_id) { todos.first.id }
      # 事前に該当のTodoレコードを削除する
      before { Todo.find(todo_id).destroy }

      example 'Todoの削除失敗' do
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 400
        expect(json['error_code']).to eq 2
        expect(json['error_message']).to eq 'リクエストの形式が不正です'
      end
    end

    context 'サーバーサイドでエラーが起きた場合' do
      let(:todo_id) { todos.first.id }

      example 'Todoの削除失敗' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(ActiveRecord::ActiveRecordError)
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 6
        expect(json['error_message']).to eq '削除に失敗しました'
      end
    end

    context 'その他のサーバーサイドでエラーが起きた場合' do
      let(:todo_id) { todos.first.id }

      example 'Todoの削除失敗' do
        allow_any_instance_of(Todo).to receive(:destroy!).and_raise(Exception)
        delete "/todos/#{todo_id}"

        expect(response.status).to eq 500
        expect(json['error_code']).to eq 1
        expect(json['error_message']).to eq 'サーバー内で不明なエラーが発生しました'
      end
    end
  end
end